from django.contrib import messages
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.shortcuts import render, HttpResponse, redirect
from django.utils.encoding import smart_str
from .forms import ContantForm


def home(request):
    return render(request, 'index.html')


def download(request):
    file_name = 'amirvalizadeh.pdf'  # get the filename of desired excel file
    path_to_file = '/home/edoardo/django_cv/cv/static/files/'  # get the path of desired excel file
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(file_name)
    response['X-Sendfile'] = smart_str(path_to_file)
    return response


def get_contact(request):
    form = ContantForm(request.POST)
    if form.is_valid():
        subject = form.cleaned_data['subject']
        name = form.cleaned_data['name']
        email = form.cleaned_data['email']
        masages = form.cleaned_data['masages']
        recipients = ['amirvalizadeh8731@gmail.com']
        masages = masages + name
        send_mail(subject, masages, email, recipients)
    messages.success(request, f'Your account has been updated!')
    return redirect('home')
