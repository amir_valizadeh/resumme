from django import forms


class ContantForm(forms.Form):
    name = forms.CharField(max_length=100)
    subject = forms.CharField(max_length=200)
    email = forms.EmailField()
    masages = forms.CharField(max_length=450)
